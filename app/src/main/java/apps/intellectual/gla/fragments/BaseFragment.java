package apps.intellectual.gla.fragments;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import apps.intellectual.gla.App;

public class BaseFragment extends Fragment {
    private static Resources resources;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        resources = App.getApplicationResources();
    }

    public static String getStringResource(int id) {
        if (resources == null) {
            resources = App.getApplicationResources();
        }
        return resources.getString(id);
    }

    public Resources getAppResources() {
        if (resources == null) {
            resources = App.getApplicationResources();
        }
        return resources;
    }
}
