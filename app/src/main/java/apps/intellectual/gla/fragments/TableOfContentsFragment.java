package apps.intellectual.gla.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import apps.intellectual.gla.R;
import apps.intellectual.gla.activities.MainActivity;

public class TableOfContentsFragment extends BaseFragment {


    public TableOfContentsFragment() {
        // Required empty public constructor
    }

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new TableOfContentsFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_table_of_contents, container, false);
        ((MainActivity) getActivity()).getToolbar().setTitle(getStringResource(R.string.table_of_contents_toolbar_label));

        return rootView;
    }
}
