package apps.intellectual.gla;

import android.app.Application;
import android.content.res.Resources;


public class App extends Application {

    private static App app;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
    }

    public static Resources getApplicationResources() {
        return getAppInstance().getResources();
    }

    public static App getAppInstance() {
        return app;
    }
}
