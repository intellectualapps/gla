package apps.intellectual.gla.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import apps.intellectual.gla.R;
import apps.intellectual.gla.activities.BaseActivity;
import apps.intellectual.gla.fragments.TableOfContentsFragment;
import apps.intellectual.gla.utils.AppInterfaces;
import apps.intellectual.gla.utils.Constants;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, AppInterfaces.FragmentSwitchedListener {
    private DrawerLayout drawer;
    private static final int DRAWER_CLOSER_INTERVAL = 250;
    private Toolbar toolbar;
    private View headerView;
    private NavigationView navigationView;
    private String viewType = Constants.TABLE_OF_CONTENTS_VIEW_TAG;
    private Fragment frag = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent() != null) {
            if (getIntent().getExtras() != null) {
                Bundle extras = getIntent().getExtras();

            }
        }
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //headerView = navigationView.findViewById(R.id.header_view);
        //headerView.setOnClickListener(this);

        initializeFragment(viewType, savedInstanceState);
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public void initializeFragment(String viewType, Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            if (viewType != null) {
                switch (viewType) {
                    case Constants.TABLE_OF_CONTENTS_VIEW_TAG:
                        frag = TableOfContentsFragment.newInstance(null);
                        break;
                }
            } else {
                frag = TableOfContentsFragment.newInstance(null);
            }

            fragmentTransaction.replace(R.id.container, frag, frag.getClass().getSimpleName());
            fragmentTransaction.commitAllowingStateLoss();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        item.setChecked(true);
        drawer.closeDrawer(GravityCompat.START);
        switch (id) {
            case R.id.nav_table_of_contents:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.nav_table_of_images:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.nav_preliminary_content:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.nav_search:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.nav_pinned_items:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.nav_glossary:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            default:
                drawer.closeDrawer(GravityCompat.START);
                break;
        }
    }

    @Override
    public void onFragmentChanged(Fragment fragment) {

    }
}
