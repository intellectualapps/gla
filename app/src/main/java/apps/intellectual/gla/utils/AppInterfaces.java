package apps.intellectual.gla.utils;

import android.support.v4.app.Fragment;

public class AppInterfaces {
    public interface FragmentSwitchedListener {
        void onFragmentChanged(Fragment fragment);
    }

    public interface FragmentOnBackPressedListener {
        void onBackPressed();
    }

    public interface ChangeMainActivityViewListener {
        void changeView(String viewTag);
    }
}
