package apps.intellectual.gla.utils;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

import apps.intellectual.gla.App;

public class CommonUtils {
    public static void displaySnackBarMessage(View mSnackBarAnchor, String message) {
        if (mSnackBarAnchor != null && mSnackBarAnchor.getContext() != null) {
            Snackbar.make(mSnackBarAnchor, message, Snackbar.LENGTH_LONG).show();
        }
    }

    public static void displayShortToastMessage(String message) {
        Context context = App.getAppInstance().getApplicationContext();
        if (context != null) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

    public static void displayLongToastMessage(String message) {
        Context context = App.getAppInstance().getApplicationContext();
        if (context != null) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }
}
