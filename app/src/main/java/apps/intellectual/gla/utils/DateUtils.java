package apps.intellectual.gla.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 */
public class DateUtils {

    public static String getSimpleDateFormat(long timestamp, String simpleDateFormat) {
        //Convert the timestamp from seconds to milliseconds
        Date dateObject = new Date(timestamp);

        DateFormat df = new SimpleDateFormat(simpleDateFormat);
        String formattedString = df.format(dateObject);

        return formattedString;
    }
}
