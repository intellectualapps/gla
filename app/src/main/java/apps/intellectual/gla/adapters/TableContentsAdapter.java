package apps.intellectual.gla.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import apps.intellectual.gla.R;
import apps.intellectual.gla.models.ContentItem;
import apps.intellectual.gla.utils.ListUtils;

public class TableContentsAdapter extends BaseRecyclerAdapter {

    private static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tableOfContentTitle;
        ClickListener clickListener;

        public ViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);
            tableOfContentTitle = (TextView) itemView.findViewById(R.id.table_of_content_title);
            itemView.setOnClickListener(this);
            tableOfContentTitle.setOnClickListener(this);
            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, false);
        }
    }

    private ArrayList<ContentItem> tableOfContentItems;
    private ClickListener clickListener;

    public TableContentsAdapter(Context context, ClickListener clickListener) {
        super(context);
        this.clickListener = clickListener;
    }

    public void setItems(ArrayList<ContentItem> items) {
        this.tableOfContentItems = items;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (tableOfContentItems != null)
            this.tableOfContentItems.clear();
        notifyDataSetChanged();
    }

    public void addItem(ContentItem contentItem) {
        if (contentItem != null) {
            this.tableOfContentItems.add(contentItem);
            notifyDataSetChanged();
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.table_of_content_layout, parent, false);

        return new TableContentsAdapter.ViewHolder(view, this.clickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final TableContentsAdapter.ViewHolder viewHolder = (TableContentsAdapter.ViewHolder) holder;
        ContentItem contentItem = tableOfContentItems.get(position);
        if (contentItem != null) {
            //viewHolder.mPaymentOptionEmailAddress.setText(paymentOption.getEmailAddress());
        }
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(tableOfContentItems) ? 0 : tableOfContentItems.size();
    }
}